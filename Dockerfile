FROM openjdk:8-jdk-alpine
ARG PACK=target/docker-package/
ENV APPROOT="/petclinic"

RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring

COPY ${PACK}/BOOT-INF/lib ${APPROOT}/lib
COPY ${PACK}/META-INF ${APPROOT}/META-INF
COPY ${PACK}/BOOT-INF/classes ${APPROOT}

ENTRYPOINT [ "java", "-cp", \
    "/petclinic:/petclinic/lib/*",\
    "org.springframework.samples.petclinic.PetClinicApplication" ]